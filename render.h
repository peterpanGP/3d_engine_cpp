#ifndef RENDER_H_INCLUDED
#define RENDER_H_INCLUDED
#include <string>
#include <iostream>
#include "geometry.h"

class Camera_Object{

    private:

    double m_location[3];
    double m_rotation[3];
    float m_focal;
    int m_resolution [2];

    public:

    Camera_Object(double location [3], double rotation[3],float focal,int reslution [2]);
    Camera_Object(Camera_Object const& camera);

    double *getLocation ();
    void setLocation(double x,double y,double z);

    double *getRotation ();
    void setRotation(double x,double y,double z);

    float getFocal();
    void setFocal(float focal);

    int *getResolution();
    void setResolution(int x, int y);
};

int perspectiv_projection(Camera_Object camera,Vertex vertex);


#endif // RENDER_H_INCLUDED
