#include "geometry.h"
#include <vector>
#include <stack>
#include <ostream>

using namespace std;

//Vertex

Vertex::Vertex(): m_location(new vector<double>(4)){

/

Vertex::Vertex(vector<double> const& location): m_location(location){
}

Vertex::Vertex(Vertex const& vertex): m_location(vertex.m_location), m_links(vertex.m_links){

}

vector<double> Vertex::getLocation()const{
    return m_location;
}

void Vertex::setLocation(vector<double> const& location){
    m_location=location;
}

vector<Vertex*> Vertex::getLinks()const{
    return m_links;
}

void Vertex::setLinks(vector<Vertex> links){
    m_links.clear();
    for (int i(0); i<links.size(); i++){
        m_links[i]=&links[i];
    }
    vector<Vertex>::iterator i;
/*    for (i=m_links.begin(); i!=m_links.end(); i++){
        *i.append(*this);
    }
*/
}

void Vertex::append(Vertex& link){
    m_links.push_back(&link);
    link.m_links.push_back(this);
}

void Vertex::remove(Vertex& link){
    vector<Vertex*>::iterator pos_a(m_links.begin());
    vector<Vertex*>::iterator pos_b(link.m_links.begin());

    while(*pos_a!=&link){
        pos_a++;
    }
    while(*pos_b!=this){
        pos_b++;
    }
    link.m_links.erase(pos_b,pos_b);
    m_links.erase(pos_a,pos_a);
}


//Matrice homogène

HomogenMatrix3D::HomogenMatrix3D(HomogenMatrix3D const& matrix){
    for (int x(0);x<3;x++){
        for (int y(0);y<3;y++){
            m_matrix[x][y]=matrix.m_matrix[x][y];
        }
    }

}


HomogenMatrix3D::HomogenMatrix3D(){
    for (int x(0); x<4; x++){
        for (int y(0); y<4; y++){
            if (x==y){
                m_matrix[x][y]=1;       //matrice identité
            }
            else{
                m_matrix[x][y]=0;
            }
        }
    }
}

void HomogenMatrix3D::set(int x, int y, double i){
    m_matrix[x][y]=i;
}

double HomogenMatrix3D::get(int x, int y)const {
    return m_matrix[x][y];
}

void HomogenMatrix3D::operator*=(HomogenMatrix3D const& a){
    HomogenMatrix3D matrix;
    double nb;

    for (int x(0); x<4; x++){       //matrix=> vierge
        for (int y(0); y<4; y++){
            matrix.set(x,y,0);
        }
    }

    for (int y_a(0); y_a<4; y_a++){
        for(int x_b(0); x_b<4; x_b++){
            for (int x_a(0); x_a<4; x_a++){
                nb=matrix.get(x_b,y_a)+m_matrix[x_a][y_a]*a.get(x_b,x_a);       //x_a=y_b
                matrix.set(x_b,y_a,nb);
            }
        }
    }
    for (int x(0); x<4; x++){       //matrix=> vierge
        for (int y(0); y<4; y++){
            nb=matrix.get(x,y);
            m_matrix[x][y]=nb;
        }
    }
}

//Mesh







//Opérateur

HomogenMatrix3D operator* (HomogenMatrix3D const& a,HomogenMatrix3D const& b){
    HomogenMatrix3D matrix;
    double nb;

    for (int x(0); x<4; x++){       //matrix=> vierge
        for (int y(0); y<4; y++){
            matrix.set(x,y,0);
        }
    }

    for (int y_a(0); y_a<4; y_a++){
        for(int x_b(0); x_b<4; x_b++){
            for (int x_a(0); x_a<4; x_a++){
                nb=matrix.get(x_b,y_a)+a.get(x_a,y_a)*b.get(x_b,x_a);       //x_a=y_b
                matrix.set(x_b,y_a,nb);
            }
        }

    }
    return matrix;
}

bool operator==(Vertex const& a, Vertex const& b){
    if (a.getLinks()==b.getLinks() && a.getLocation()==b.getLocation()){
        return true;
    }else{
        return false;
    }
}

bool operator!=(Vertex const& a, Vertex const& b){
    if (a.getLinks()!=b.getLinks() && a.getLocation()!=b.getLocation()){
        return true;
    }else{
        return false;
    }
}
