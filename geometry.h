#ifndef GEOMETRY_H_INCLUDED
#define GEOMETRY_H_INCLUDED
#include <vector>
#include <stack>
#include <ostream>

class HomogenMatrix3D {
    private:

    double m_matrix [4][4];

    public:

    HomogenMatrix3D();
    HomogenMatrix3D(HomogenMatrix3D const& matrix);

    void set (int x,int y,double i);
    double get (int x,int y)const;

    void operator*= (const HomogenMatrix3D & a);
};

class Vertex{

    private:

    std::vector<double> m_location;
    std::vector<Vertex*> m_links;

    public:

    Vertex();
    Vertex(std::vector<double> const& location);
    Vertex(Vertex const& vertex);

    std::vector<double> getLocation ()const;                 // get et set pour m_location
    void setLocation(std::vector<double> const& location);

    std::vector<Vertex*> getLinks()const;                     // get et set pour m_link
    void setLinks(std::vector<Vertex> links);

    void append(Vertex& link);                           // ajouter ou enlever des links
    void remove(Vertex& link);


};

class Mesh {

    private:

    std::vector<Vertex> m_vertex;
    std::stack<HomogenMatrix3D> m_saveMatrix;
    std::vector<double> m_location;
    std::vector<double> m_rotation;

    public:

    Mesh();
    Mesh(std::vector<double> const& location, std::vector<double> const& rotation, std::vector<Vertex> const& vertex);
    Mesh(Mesh const& mesh);

    void translate(double x, double y,double z);
    void rotate(double x, double y, double z);
    void apply(HomogenMatrix3D matrix);

    void save();
    void pop(int save);

    std::vector<Vertex> getVertex();
    void setVertex(std::vector<Vertex> vertex);
    std::stack<HomogenMatrix3D> getSaveMatrix();
    std::vector<double> getLocation();
    void setLocation(std::vector<double> location);
    std::vector<double> getRotation();
    void setRotation(std::vector<double> rotation);

};

//opérateur

HomogenMatrix3D operator* (HomogenMatrix3D const& a,HomogenMatrix3D const& b);
std::vector<double> operator* (const HomogenMatrix3D & a, const std::vector<double> & b);

bool operator==(Vertex const& a, Vertex const& b);
bool operator!=(Vertex const& a, Vertex const& b);

#endif // GEOMETRY_H_INCLUDED
